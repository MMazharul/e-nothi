@extends('frontend.layouts.master')

@section('title', 'Blog Details')

@section('content')
    <!-- Title -->
    <h1 class="mt-4">{{ $file->title }}</h1>

    <!-- Author -->
    <p class="lead">
        by <a href="#">{{ $file->creator->name }}</a>
    </p>

    <!-- Date/Time -->
    <p>Posted on {{ $file->created_at }}</p>

    <hr>

    <!-- Preview Image -->
    @if(!is_null($file->image))
        <img class="img-fluid rounded" src="{{ asset('uploads/files/'.$file->image) }}" alt="">
    @endif
    <hr>

    <!-- Post Content -->
    {!! $file->description !!}
    <hr>

    @if(auth()->check())
    <!-- Comments Form -->
    <div class="card my-4">
        <h5 class="card-header">Add note:</h5>

        <div class="card-body">
            {{ Form::open(['route' => ['comment', $file->id]]) }}
            {{ Form::hidden('commentable_type', 'File') }}
            <div class="form-group">
                 <textarea id="full-featured" class="form-control" name="body"></textarea>
                    {{--<textarea name="body" class="form-control" rows="3"></textarea>--}}
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
    @else
        <a href="{{ route('login') }}"><button  class="btn btn-primary">Login To Leave A Comment</button></a>
    @endif

    <!-- Single Comment -->

    @foreach($file->comments as $comment)
    <div class="media mb-4">
        {{--<img class="d-flex mr-3 rounded-circle" src="{{ asset('uploads/users/'.$comment->commentedBy->profile->picture) }}" alt="">--}}
        <div class="media-body">
            <h5 class="mt-0">{{ $comment->commentedBy->name }} <small>{{ $comment->created_at->toFormattedDateString() }} <mark>{{ $comment->created_at->diffForHumans() }}</mark></small></h5>
            {!! $comment->body !!}
        </div>
    </div>
    @endforeach
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/tinymce.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/jquery.tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/plugins/spellchecker/plugin.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea#full-featured',
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tiny.cloud/css/codepen.min.css'
            ],
            image_class_list: [
                { title: 'None', value: '' },
                { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
                }
            },
            templates: [
                { title: 'Some title 1', description: 'Some desc 1', content: 'My content' },
                { title: 'Some title 2', description: 'Some desc 2', content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>' }
            ],
            template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
            image_caption: true,

            spellchecker_dialog: true,
            spellchecker_whitelist: ['Ephox', 'Moxiecode']
        });
    </script>
@endpush
