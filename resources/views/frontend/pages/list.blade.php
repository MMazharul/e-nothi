@extends('frontend.layouts.master')

@section('title', 'Home')

@section('content')
    <br>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </nav>
    {{--<h1 class="my-4">Page Heading--}}
        {{--<small>Secondary Text</small>--}}
    {{--</h1>--}}

    <!-- Blog Post -->
    @foreach($files as $file)
    <div class="card mb-4">
            @if(!is_null($file->image))
                <img class="img-fluid rounded" src="{{ asset('uploads/files/'.$file->image) }}" alt="">
            @endif
            <div class="card-body">
            <h2 class="card-title">{{ $file->title }}</h2>
            <p class="card-text">{!! str_limit($file->summary , 100) !!}</p>
            <a href="{{ route('singlePage', $file->id) }}" class="btn btn-primary float-right">Read More &rarr;</a>
        </div>
        <div class="card-footer text-muted">
            Posted on {{ $file->created_at->toFormattedDateString() }} by
            <a href="#">{{ $file->creator->name }}</a>
        </div>
    </div>
    @endforeach

    <!-- Pagination -->
    {{--{{ $files->links() }}--}}
@endsection
