@extends('backend.layouts.master')

@section('content')
    <!--// top-bar -->
    <div class="container-fluid">
        <div class="row">
            <!-- Stats -->
            <div class="outer-w3-agile col-xl">
                <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-primary">
                    <div class="s-l">
                        <h5>Files</h5>
                        {{--<p class="paragraph-agileits-w3layouts text-white">Lorem Ipsum</p>--}}
                    </div>
                    <div class="s-r">
                        <h6 >{{ $totalFiles }}
                            <a href="{{url('/directroy/files')}}"> <i class="far fa-edit"></i></a>
                        </h6>
                    </div>
                </div>

                <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-success">
                    <div class="s-l">
                        <h5>Directories</h5>
                        {{--<p class="paragraph-agileits-w3layouts">Lorem Ipsum</p>--}}
                    </div>
                    <div class="s-r">
                        <h6>{{ $totalDirectories }}
                            <i class="fas fa-tasks"></i>
                        </h6>
                    </div>
                </div>
                <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-danger">
                    <div class="s-l">
                        <h5>Users</h5>
                        {{--<p class="paragraph-agileits-w3layouts">Lorem Ipsum</p>--}}
                    </div>
                    <div class="s-r">
                        <h6>{{ $totalUsers }}
                            <i class="far fa-smile"></i>
                        </h6>
                    </div>
                </div>
            </div>
            <!--// Stats -->
        </div>
    </div>
    <!-- Simple-chart -->
@endsection
