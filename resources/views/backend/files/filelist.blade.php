@extends('backend.layouts.master')

@section('content')
    <div class="card">
        <div class="card-header bg-success">
            <div class="float-left text-white">
                Files
            </div>

        </div>
        <div class="card-body">
            {!! Form::open(['route' => 'files.index', 'method'=>'GET']) !!}
            <div class="input-group mb-2 mr-sm-2">
                {!! Form::text('keyword', null ,[
                                                    'class' => 'form-control'
                                                ]) !!}
                <div class="input-group-append">
                    <div class="input-group-text">{{ Form::button('<i class="fa fa-search"></i>', ['type' => 'submit']) }}</div>
                </div>
            </div>
            {!! Form::close() !!}

            @include('backend.layouts.elements.error')

            <table class="table table-bordered table-striped">
                <tr>
                    <th width="100">SL#</th>
                    <th>Title</th>
                    <th width="200" class="text-right">Action</th>
                </tr>
                @php $serial=0; @endphp
                @foreach($files as $file)
                    <tr>
                        <td>{{ ++$serial }}</td>
                        <td>{{ $file->title }}</td>
                        <td class="text-right">
                            <a href="{{ url('/directory/file/show', $file->id) }}" class="btn btn-sm btn-primary">Show</a>

                        </td>
                    </tr>
                @endforeach
            </table>
            <div class="float-right">
                {{--{{ $files->links() }}--}}
            </div>
        </div>
    </div>
@endsection
