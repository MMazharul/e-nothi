@extends('backend.layouts.master')

@section('content')
<div class="card">
    <div class="card-header bg-success">
        <div class="float-left text-white">
            Create File
        </div>
        <div class="float-right">
            <a href="{{ route('files.index') }}" class="btn btn-primary">List</a>
        </div>
    </div>
    <div class="card-body">
        @include('backend.layouts.elements.error')
        {!! Form::open(['route' => 'files.store', 'files' => true]) !!}
        @include('backend.files.form')
        <div class="form-group row">
            <div class="col-sm-10 text-center">
                {!! Form::button('Add', [
                                            'class' => 'btn btn-primary',
                                            'type' => 'submit',
                                        ]) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
