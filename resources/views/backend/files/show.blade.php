@extends('backend.layouts.master')

@section('content')

<div class="card">
    <div class="card-header bg-success">

        <div class="float-left text-white">
            File Details
        </div>

        <div class="float-right">
            <a href="{{ route('files.index') }}" class="btn btn-primary">List</a>
        </div>

    </div>
    <div class="card-body">

        <table class="table table-bordered table-striped">

            <tr>
                <td>Title</td>
                <td>{{ $file->title }}</td>
            </tr>

            <tr>
                <td>Directory</td>
                <td>{{ $file->directory->title }}</td>
            </tr>

            <tr>
                <td>Image</td>
                <td><img src="{{ asset('/uploads/files/'. $file->image) }}" alt=""></td>
            </tr>
            
            <tr>
                <td>Description</td>
                <td>{!! $file->description !!}</td>
            </tr>

            <tr>
                <td>Created At</td>
                <td>{{ $file->created_at }}</td>
            </tr>

            <tr>
                <td>Created By</td>
                <td>{{ $file->creator->name }}</td>
            </tr>

        </table>

        @foreach($allNotes as $note)

        <div class="col-md-12">
           <h5>{{$note->created_by}}</h5><small>{{ $note->created_at }}</small>
            <p class="mt-2">{{ $note->description }}</p>
        </div>
            <hr>
        @endforeach

        {!! Form::open(['route' => 'note',  'files' => true]) !!}
        <div class="form-group">
            {!! Form::label('Comment', null, ['class' => 'col-sm-2 col-form-label']) !!}

            <div class="col-md-12">
                {{--<textarea id="full-featured" class="form-control" name="body"></textarea>--}}

                <input type="hidden" name="file_id" value="{{ $file->id }}">

                {!! Form::textarea('description', null, [
                      'class' => 'form-control',
                      'rows' => '3'
                  ]) !!}
                @if ($errors->has('description'))
                    <span class="text-danger" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
                @endif
            </div>


            <button class="btn btn-primary mt-2 ml-3">Comment</button>


            {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection




