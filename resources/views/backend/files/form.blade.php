<div class="form-group row">
    {!! Form::label('Title', null, ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('title', null, [
                'class' => 'form-control',
                'id' => 'Title',
            ]) !!}
        @if ($errors->has('title'))
            <span class="text-danger" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('directory_id', 'Select Derectory', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        @foreach ($directories as $id => $title)
            <div class="checkbox">
                <label>
                    {{ Form::checkbox('directory_id[]',$id,in_array($id, $selected_directories), ['class' => 'field']) }}
                    {{ $title }}
                </label>
            </div>
        @endforeach
            @if ($errors->has('directory_id'))
                <span class="text-danger" role="alert">
                <strong>{{ $errors->first('directory_id') }}</strong>
            </span>
            @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('description', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {{--<textarea id="full-featured" class="form-control" name="body"></textarea>--}}
        {!! Form::textarea('description', null, [
                'class' => 'form-control',
                'id' => 'full-featured',
            ]) !!}
        @if ($errors->has('description'))
            <span class="text-danger" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('image', null, ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::file('image', [
                'class' => 'form-control',
                'id' => 'image',
            ]) !!}<br>
        @if ($errors->has('image'))
            <span class="text-danger" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--<div class="form-group row">--}}
    {{--{!! Form::label('tag_id', 'Directories', ["class" => "col-md-2 col-form-label text-md-right"]) !!}--}}
    {{--<div class="col-md-10">--}}
        {{--@foreach ($directories as $directory)--}}
            {{--<div class="checkbox">--}}
                {{--<label>--}}
                    {{--{{ Form::checkbox('directory_ids[]', $directory->id, in_array($directory->title, $selectedDirectory), ['class' => 'field']) }}--}}
                    {{--{{ $directory->title }}--}}
                {{--</label>--}}
            {{--</div>--}}
        {{--@endforeach--}}
    {{--</div>--}}
{{--</div>--}}

{{--in_array($directory->id, $selectedTagIds--}}

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/tinymce.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/jquery.tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/plugins/spellchecker/plugin.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea#full-featured',
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
            toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tiny.cloud/css/codepen.min.css'
            ],
            image_class_list: [
                { title: 'None', value: '' },
                { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
                }
            },
            templates: [
                { title: 'Some title 1', description: 'Some desc 1', content: 'My content' },
                { title: 'Some title 2', description: 'Some desc 2', content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>' }
            ],
            template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
            image_caption: true,

            spellchecker_dialog: true,
            spellchecker_whitelist: ['Ephox', 'Moxiecode']
        });
    </script>
@endpush



