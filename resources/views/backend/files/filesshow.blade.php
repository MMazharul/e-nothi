@extends('backend.layouts.master')

@section('content')

    <div class="card">
        <div class="card-header bg-success">

            <div class="float-left text-white">
                File Details
            </div>

            {{--<div class="float-right">--}}
                {{--<a href="{{ route('files.index') }}" class="btn btn-primary">List</a>--}}
            {{--</div>--}}

        </div>
        <div class="card-body">

            <table class="table table-bordered table-striped">

                <tr>
                    <td>From</td>
                    <td>{{ $directory }}</td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td>{{ $file->title }}</td>
                </tr>

                {{--<tr>--}}
                    {{--<td>Directory</td>--}}
                    {{--<td>{{ $file->directory->title }}</td>--}}
                {{--</tr>--}}

                <tr>
                    <td>Image</td>
                    <td><img style="width:300px;height:150px" src="{{ asset('/uploads/files/'. $file->image) }}" alt=""></td>
                </tr>

                <tr>
                    <td>Description</td>
                    <td>{!! $file->description !!}</td>
                </tr>

                <tr>
                    <td>Created At</td>
                    <td>{{ $file->created_at }}</td>
                </tr>

                <tr>
                    <td>Created By</td>
                    <td>{{ $file->creator->name }}</td>
                </tr>

            </table>

            @foreach($comments->comments as $comment)

                <div class="col-md-12">
                    <h5>{{\Illuminate\Support\Facades\Auth::user()->name}}</h5>
                    <p class="mt-2">{{ $comment->body }}</p>
                    <small>{{ $comment->created_at->diffForHumans() }}</small>
                </div>
                <hr>
            @endforeach

            {!! Form::open(['url' => url('/file/comment'),  'files' => true]) !!}
            <div class="form-group" id="comments">
                {!! Form::label('Comment', null, ['class' => 'col-sm-2 col-form-label']) !!}

                <div class="col-md-12">
                    {{--<textarea id="full-featured" class="form-control" name="body"></textarea>--}}

                    <input type="hidden" name="file_id" value="{{ $file->id }}">

                    {!! Form::textarea('body', null, [
                          'class' => 'form-control',
                          'rows' => '3'
                      ]) !!}
                    @if ($errors->has('body'))
                        <span class="text-danger" role="alert">
                <strong>{{ $errors->first('body') }}</strong>
            </span>
                    @endif
                </div>


                <button class="btn btn-primary mt-2 ml-3">Comment</button>


                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection




