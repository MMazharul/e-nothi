@extends('backend.layouts.master')

@section('content')
<div class="card">
    <div class="card-header bg-success">
        <div class="float-left text-white">
            Directories
        </div>
        <div class="float-right">
{{--            <a href="{{ route('directories.trash') }}" class="btn btn-primary">Trash</a>--}}
            <a href="{{ route('directories.create') }}" class="btn btn-primary">Add New</a>
        </div>
    </div>
    <div class="card-body">
        {!! Form::open(['route' => 'directories.index', 'method'=>'GET']) !!}
            <div class="input-group mb-2 mr-sm-2">
                {!! Form::text('keyword', null ,[
                                                    'class' => 'form-control'
                                                ]) !!}
                <div class="input-group-append">
                    <div class="input-group-text">{{ Form::button('<i class="fa fa-search"></i>', ['type' => 'submit']) }}</div>
                </div>
            </div>
        {!! Form::close() !!}
        @include('backend.layouts.elements.message')
        <table class="table table-bordered table-striped">
            <tr>
                <th width="100">SL#</th>
                <th>Title</th>
                <th>Created By</th>
                <th width="200" class="text-right">Action</th>
            </tr>
            @foreach($directories as $directory)
                <tr>
                    <td>{{ ++$serial }}</td>
                    <td>{{ $directory->title }}</td>
                    <td>{{ $directory->creator->name }}</td>
                    <td class="text-right">
{{--                        <a href="{{ route('directories.show', $directory->id) }}" class="btn btn-sm btn-primary">Show</a>--}}
                        <a href="{{ route('directories.edit', $directory->id) }}" class="btn btn-sm btn-warning">Edit</a>
                                            {{--<a href="{{ route('directories.destroy', $directory->id) }}">--}}


                        {!! Form::open([
                                        'route' => ['directories.destroy', $directory->id],
                                        'method' => 'delete',
                                        'style' => 'display:inline',
                                    ]) !!}
                        {!! Form::button('Delete',[
                                'type' => 'submit',
                                'class' => "btn btn-sm btn-danger",
                                'onclick' => 'return confirm("Are You Sure Want To Delete ?")',

                        ]) !!}
                        {!! Form::close() !!}
                        {{--</a>--}}
                    </td>
                </tr>
            @endforeach
        </table>
        <div class="float-right">
            {{ $directories->links() }}
        </div>
    </div>
</div>
@endsection
