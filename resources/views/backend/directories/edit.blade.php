@extends('backend.layouts.master')

@section('content')
    <div class="card">
        <div class="card-header bg-success">
            <div class="float-left text-white">
                Edit Directory
            </div>
            <div class="float-right">
                <a href="{{ route('directories.index') }}" class="btn btn-primary">List</a>
            </div>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            {!! Form::model($directory,[
                        'route' => ['directories.update', $directory->id],
                        'method' => 'put'
                        ]) !!}
                @include('backend.directories.form')
                <div class="form-group row">
                    <div class="col-sm-10 text-center">
                        {!! Form::button('Update', [
                                                    'class' => 'btn btn-primary',
                                                    'type' => 'submit',
                                                ]) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
