<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Title', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, [
                'placeholder' => 'Directory',
                'class' => 'form-control',
                'id' => 'Title',
            ]) !!}<br>
        @if ($errors->has('title'))
            {{ $errors->first('title') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('file_id', 'Select Derectory', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        @foreach ($files as $id => $title)
            <div class="checkbox">
                <label>
                    {{ Form::checkbox('file_id[]',$id,in_array($id, $selected_files), ['class' => 'field']) }}
                    {{ $title }}
                </label>
            </div>
        @endforeach
        @if ($errors->has('file_id'))
            <span class="text-danger" role="alert">
                <strong>{{ $errors->first('file_id') }}</strong>
            </span>
        @endif
    </div>
</div>

