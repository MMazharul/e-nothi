<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['file_id', 'description', 'image', 'created_by','modified_by'];

    public function notes()
    {
        return $this->hasMany(Note::class);
    }
}
