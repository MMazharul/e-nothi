<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Pondit\Authorize\Models\Authorizable;

class User extends Authenticatable
{
    use Notifiable;
    use Authorizable;

    const ADMIN_ROLE_ID = 1;
    const GUEST_ROLE_ID = 2;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password', 'role_id','directory_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function directories()
    {
        return $this->belongsToMany(Directory::class);
    }
}
