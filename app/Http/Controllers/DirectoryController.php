<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Directory;
use App\Http\Requests\DirectoryRequest;
use App\User;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class DirectoryController extends Controller
{

    public function index()
    {
        $paginatePerPage = 5;
        $pageNumber = request('page');
        if(!is_null($pageNumber)){
            $serial = $paginatePerPage * $pageNumber - $paginatePerPage;
        }else{
            $serial = 0;
        }
        $keyword = request('keyword');
        if(!is_null($keyword)){
            $directories = Directory::where('title', 'Like', "%{$keyword}%")->paginate($paginatePerPage);
        }else{
            $directories = Directory::paginate($paginatePerPage);
        }
        return view('backend.directories.index', compact('directories', 'serial'));
    }

    public function create()
    {
        $files=\App\File::pluck('title','id');

        $selected_files = [];
        return view('backend.directories.create',compact('files','selected_files'));
    }

    public function store(DirectoryRequest $request)
    {
        $data = $request->only('title');
        $data['created_by'] = auth()->user()->id;
        $directory=Directory::create($data);

        $file_ids=$request->input('file_id');

        $directory->files()->attach($file_ids);
//        Notification::send(auth()->user(), new UserRegistered($directory));
        Session::flash('message', 'Created Successfully');
        return redirect()->route('directories.index');
    }

    public function show(Directory $directory)//dependency injection or route model binding
    {
        return view('backend.directories.show', compact('directory'));
    }

    public function edit(Directory $directory)
    {
        $files = \App\File::pluck('title', 'id');

        $selected_files = $directory->files()->pluck('id')->toArray();
        return view('backend.directories.edit', compact('directory','selected_files','files'));
    }

    public function update(DirectoryRequest $request, Directory $directory)
    {
        $data = $request->only('title');

        $directory->update($data);

        $file_ids=$request->input('file_id');

        $directory->files()->sync($file_ids);

        Session::flash('message', 'Updated Successfully');
        return redirect()->route('directories.index');
    }

    public function destroy(Directory $directory)
    {
        $directory->delete();
        Session::flash('message', 'Deleted Successfully');
        return redirect()->route('directories.index');
    }

    public function directoryFiles()
    {

        $directotyId=Auth::user()->directories()->pluck('id')->first();
        $files=Directory::find($directotyId)->files()->get();
        return view('backend/files/filelist',compact('files'));
    }


    public function directoryFileShow($id)
    {
       $comments=\App\File::with([
           'comments'=>function($query)
           {
               $query->orderBy('comments.created_at','desc');
           }
       ])->find($id);

        $file=\App\File::find($id);
        $directory=User::find(Auth::user()->id)->directories()->pluck('title')->first();
       return view('backend.files.filesshow',compact('file','directory','comments'));



    }

}
