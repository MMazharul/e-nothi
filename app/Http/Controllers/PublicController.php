<?php

namespace App\Http\Controllers;

use App\Category;
use App\Directory;
use App\File;
use App\Post;
use App\Tag;
use App\Video;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {

        $files = File::whereIn('directory_id', auth()->user()->directories()->pluck('id'))
                        ->orderBy('created_at', 'desc');


        $data = [
            'files' => $files,
            'directories' => auth()->user()->directories()->get(),
        ];
        return view('frontend.pages.list', $data);
    }


    public function singlePage(File $file)
    {
        $data = [
            'directories' => auth()->user()->directories()->toArray(),
            'file' => $file,
        ];
        return view('frontend.pages.single', $data);
    }

    public function listPage($id)
    {

        $files = [];
        $directory = Directory::findOrFail($id);
        if($directory){
            $files = $directory->files()->paginate(10);
        }
        $data = [
            'files' => $files,
            'directories' => auth()->user()->directories()->toArray(),
        ];
        return view('frontend.pages.list', $data);
    }
}
