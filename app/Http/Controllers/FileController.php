<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Directory;
use App\File;
use App\Http\Requests\FileRequest;
use App\Note;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Image;

use URL;

class FileController extends Controller
{
    const UPLOAD_DIR = '/uploads/files/';

    public function index()
    {
        $paginatePerPage = 5;
        $pageNumber = request('page');

        if(!is_null($pageNumber)){
            $serial = $paginatePerPage * $pageNumber - $paginatePerPage;
        }else{
            $serial = 0;
        }

        $keyword = request('keyword');
        if(!is_null($keyword)){
            $files = File::latest()->where('title', 'Like', "%{$keyword}%")->paginate($paginatePerPage);
        }else{
            $files = File::latest()->paginate($paginatePerPage);
        }
        return view('backend.files.index', compact('files', 'serial'));
    }

    public function create()
    {

        $directories = Directory::pluck('title', 'id');
        $selected_directories = [];

        return view('backend.files.create', compact('directories','selected_directories'));
    }

    public function store(FileRequest $request)
    {
        $data = $request->only('image','title','description');

        $data['created_by'] = auth()->user()->id;
        if($request->hasFile('image')){
            $data['image'] = $this->uploadImage($request->image);
        }

        $file = File::create($data);
        $director_ids=$request->input('directory_id');

        $file->directories()->attach($director_ids);
        Session::flash('message', 'Created Successfully');
        return redirect()->route('files.index');


    }

    public function show(File $file)//dependency injection or route model binding
    {
        $allNotes=Note::where('file_id', $file->id)->get();
        return view('backend.files.show', compact('allNotes','file'));
    }

    public function edit(File $file)
    {
         $directories = Directory::pluck('title', 'id');

         $selected_directories = $file->directories()->pluck('id')->toArray();


        return view('backend.files.edit', compact('file', 'directories','selected_directories'));
    }

    public function update(FileRequest $request, File $file)
    {

        $data = $request->only('title','image','description');

        $data['updated_by'] = auth()->user()->id;
        if($request->hasFile('image')){
            $data['image'] = $this->uploadImage($request->image);
        }
        $file->update($data);

        $director_ids=$request->input('directory_id');

        $file->directories()->sync($director_ids);
//        $file->tags()->sync($request->tag_ids);
        Session::flash('message', 'Updated Successfully');
        return redirect()->route('files.index');

    }

    public function destroy(File $file)
    {
//        File::destroy($id);
        $this->unlink($file->image);
//        $file->tags()->detach();
        $file->delete();
        Session::flash('message', 'Deleted Successfully');
        return redirect()->route('files.index');
    }

    private function uploadImage($file)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
        $file_name =  $timestamp.'.'.$file->getClientOriginalExtension();
        Image::make($file)->resize(750, 300)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)) {
            @unlink(public_path() . self::UPLOAD_DIR . $file);
        }
    }

    public function fileComment(Request $request)
    {

        try {
            $file_id = $request->input('file_id');
//            return $file_id;

            $file = File::findOrFail($file_id);

            $fileComment = new Comment();
            $fileComment->created_by=Auth::user()->id;
            $fileComment->body = $request->input('body');

            $file->comments()->save($fileComment);
            Session::flash('message', 'file added!');
            return Redirect::to(URL::previous() . "#comments");
        } catch(Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }
}
