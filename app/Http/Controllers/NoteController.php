<?php

namespace App\Http\Controllers;

use App\File;
use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {

//        $data = File::find($file_id);
        $note  = new Note();
        $note->file_id = $request->file_id;
        $note->description = $request->description;
        $note->created_by = Auth::user()->name;
        $note->save();

//        $data = $request->all();
//        Note::create($data);




        return redirect()->back();
    }
}
