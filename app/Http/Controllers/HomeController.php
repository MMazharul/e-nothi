<?php

namespace App\Http\Controllers;

use App\Directory;
use App\File;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $directoryId=User::find(Auth::user()->id)->directories()->pluck('id')->first();


        $totalUser=0;
        if($directoryId!=null)
        {
            $totalUser=Directory::find($directoryId)->users()->count();
        }

        $files=0;
        if($directoryId!=null)
        {
            $files=Directory::find($directoryId)->files()->count();
        }


        $data = [
            'totalFiles' => $files,
            'totalDirectories' => Directory::count(),
            'totalUsers' => $totalUser,
        ];
        return view('backend.home', $data);
    }
}
