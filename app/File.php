<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'title',
        'directory_id',
        'description',
        'image',
        'created_by',
    ];

    public function directories()
    {
        return $this->belongsToMany(Directory::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }



    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

//    public function tags()
//    {
//        return $this->belongsToMany(Tag::class);
//    }

//    public function directories()
//    {
//        return $this->belongsToMany(Directory::class);
//    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->orderBy('created_at', 'desc');
    }
}
