<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DirectoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $directories = [
            [
                'title' => 'Test Directory 1',
                'created_by' => \App\User::ADMIN_ROLE_ID,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ];
        DB::table('directories')->insert($directories);
    }
}
