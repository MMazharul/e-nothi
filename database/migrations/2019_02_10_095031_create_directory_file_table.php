<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectoryFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directory_file', function (Blueprint $table) {
            $table->integer('directory_id')->unsigned();
            $table->integer('file_id')->unsigned();
            $table->primary(['directory_id', 'file_id']);
            $table->foreign('directory_id')->references('id')->on('directories')->onDelete('cascade');
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directory_file');
    }
}
