<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectoryUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directory_user', function (Blueprint $table) {
            $table->integer('directory_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('directory_id')->references('id')->on('directories');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('directory_user', function (Blueprint $table) {
            Schema::dropIfExists('directory_user');
        });
    }
}
